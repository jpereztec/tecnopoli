$(document).ready(function() {
	$('a[href*=#]').click(function() {
	    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
	        var $target = $(this.hash);
	        $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
	        if ($target.length) {
	            var targetOffset = $target.offset().top;
	            $('html,body').animate({
	                scrollTop: targetOffset
	            }, 1000);
	            return false;
	        }
	    }
	});
	$.fn.tecnoMailerOptions.ga = ['sitio', 'contacto'];
	$('.contact').tecnoMailer({validateOptions:{
	    rules: {
	            'p[name]':{
	                lettersonly: true,
	                required: true,
	            },
	            'p[email]':{
	                email: true,
	                required: true,
	            },
	            'p[phone]':{
	                phoneMX: true,
	                required: true,
	            },
	            'p[msg]':{
	                required: true,
	            },
	        }
	}});
	$('.form-horizontal').tecnoMailer({validateOptions:{
    rules: {
            'p[name]':{
                lettersonly: true,
                required: true,
            },
            'p[email]':{
                email: true,
                required: true,
            },
            'p[phone]':{
                phoneMX: true,
                required: true,
            },
            'p[lineas]':{
                required: true,
            },
            'p[agentes]':{
                required: true,
            },
            'p[msg]':{
                required: true,
            },
        }
}});
});	
